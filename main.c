#include <stdio.h>

int main() {

    float a;
    float b;

    printf("\nTaschenrechner (Addierer) \n\n");
    printf("Gib die erste Zahl ein: \n");
    scanf("%f", &a );                               //erste Zahl einlesen und "a" zuweisen
    printf("Gib die zweite Zahl ein! \n");
    scanf("%f", &b);                                //zweite Zahl einlesen und "a" zuweisen
    printf("%f + %f = %.2f \n", a, b, a + b);       //%.2f fuer zwei Nachkommastellen

    return 0;
}
